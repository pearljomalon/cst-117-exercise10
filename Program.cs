using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise10
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "Exercise10.txt";

            StreamReader inputFile = File.OpenText(path);

            List<string> singleWords = new List<string>();
            List<string> fileText = new List<string>();

            string[] tokens;

            int counter = 0;

            while (!inputFile.EndOfStream)
            {
                fileText.Add(inputFile.ReadLine());
            }

            foreach(string str in fileText)
            {
                tokens = str.Split(null);

                singleWords.AddRange(tokens);
            }

            foreach (string str in singleWords)
            {
                if (str.ToLower().EndsWith("t") || str.ToLower().EndsWith("e"))
                    counter++;
            }

            Console.WriteLine(String.Format("There are {0} words that end in 't' or 'e'", counter));

            Console.WriteLine("Press any key to exit...");

            Console.ReadLine();
        }
    }
}
